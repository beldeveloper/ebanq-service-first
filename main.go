package main

import "net/http"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("First: master"))
	})
	http.ListenAndServe(":7001", nil)
}
